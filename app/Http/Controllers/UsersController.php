<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function index() {
        return view('modules.user.user-file-upload');
    }
    public function upload(Request $request) {

        $response = [];
        if ($request->file('file')) {
            $ext = $request->file('file')->extension();
            $mime_type = $request->file('file')->getMimeType();
            $file_name = $request->file('file')->getClientOriginalName();
            $validate = Validator::make($request->all(), [
                'file' => 'required|mimes:jpeg,jpg,pdf,mp4|max:25000'
            ]);
            if (!$validate->fails()) {
                try {
                    $filename = 'goreact-'.time().'-'.rand(0, 9999999);
                    $file = 'public/goreact/'.$filename.'.'.$ext;
                    $result =  Storage::disk('s3')->put($file, file_get_contents($request->file), 'public');
                    if ($result) {
                        $response['path'] = env('AWS_STORAGE_PATH', '').$file;
                        $response['type'] = $mime_type;
                        $response['filename'] = $file_name;
                        $response['status'] = 1;
                    } else {
                        $response['status'] = 0;
                        $response['message'] = ['Somethings went wrong!'];
                    }
                } catch (\Exception $e) {
                    $response['status'] = 0;
                    $response['message'] = $e->getMessage();
                }
            } else {
                $response['status'] = 0;
                $response['errors'] = $validate->errors()->get('file');
            }
        } else {
            $response['status'] = 0;
        }

        return $response;
    }
}
