<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UploadFileTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function uploadFile() {
        Storage::fake('s3');
 
        $this->json('post', '/upload', [
            'file' => $file = UploadedFile::fake()->image('random.png')
        ]);
 
        $this->assertEquals('file/' . $file->hashName(), Upload::latest()->first()->file);
 
        Storage::disk('s3')->assertExists('file/' . $file->hashName());
    }
    
}
