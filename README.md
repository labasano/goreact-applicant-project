# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### REPOSITORY 
git clone https://labasano@bitbucket.org/labasano/goreact-applicant-project.git

* LARAVEL AND VUE LOCAL DEVELOPMENT SERVER MUST BE RUN TOGETHER

### ASSUMING COMPOSER AND NODE JS WAS ALREADY INSTALLED
### INSTALL DEPENDENCIES

### IF CLONING  USING THE LINK
### GO TO APP MAIN DIRECTORY
cd goreact-applicant-project

npm install
composer install

### RUN LOCAL DEVELOPMENT SERVER
php artisan serve

-----------------------------------------------------

### VUE BUILD
### RUN DEVELOPMENT MODE --WATCH
npm run watch

### BUILD PRODUCTION  MODE --DEV
npm run dev

### BUILD PRODUCTION MODE
npm run production

### CYPRESS UNIT
npx cypress open

### LARAVEL UNIT TEST
php artisan test



