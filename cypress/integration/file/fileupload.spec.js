describe('File Upload', () => {
    it('Visit App', () => {
      cy.visit('http://127.0.0.1:8000/')
    })
    beforeEach(() => {
        Cypress.Cookies.preserveOnce('laravel_session', 'laravel_token')
    })
    it('IS Spinner is working properly', () => {
      cy.get('#upload-spinner').should('have.class', 'hide')
      cy.get('#upload-spinner').should('not.have.class', 'show')

      cy.get('#upload-spinner').click({ force : true });
      cy.wait(500)
  
    })
})