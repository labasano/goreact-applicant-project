require('./bootstrap');
import Vue from 'vue';
// axios
import axios from 'axios';
import VueAxios from 'vue-axios';


window.axios = require('axios');
Vue.prototype.$http = window.axios;
window.Vue = Vue;
window.Vue.config.productionTip = false;
Vue.use(VueAxios, axios);


import VueFileAgent from 'vue-file-agent';
Vue.use(VueFileAgent);

Vue.component('user-file-upload', require('./modules/users/FileUpload.vue').default);


var app = new Vue({
    el: '#app'
});

