@extends('layouts.app')
@section('content')
    <user-file-upload inline-template>
        <div>
            <div class="container">
            <div class="row pt-3">
                <div class="col-12">
                    <vue-file-agent
                        ref="vueFileAgent"
                        :theme="'list'"
                        :multiple="false"
                        :deletable="true"
                        :meta="true"
                        :accept="'.jpg,.mp4,.pdf'"
                        :maxSize="'25MB'"
                        :maxFiles="1"
                        :helpText="'Choose images or mp4 files'"
                        :errorText="{
                        type: 'Invalid file type. Only images or zip Allowed',
                        size: 'Files should not exceed 10MB in size',
                        }"
                        @select="filesSelected($event)"
                        @beforedelete="onBeforeDelete($event)"
                        @delete="fileDeleted($event)"
                        @upload="onUpload($event)"
                        @upload:error="onUploadError($event)"
                        v-model="fileRecords"
                    ></vue-file-agent>
                    <button class="btn btn-primary btn-block mt-3 upload-button" :class="{ hide : fileRecords.length == 0}" @click="uploadFiles()">
                        <span v-if="!uploading">Upload</span>
                        <span v-else>Uploading...</span>
                        <span :class="{'hide' : !uploading, 'show' : uploading}" class="spinner-border spinner-border-sm" id="upload-spinner" role="status" aria-hidden="true"></span>
                    </button>
                    <div  v-for="item in errors" class="alert alert-danger mt-2" role="alert">
                        @{{item}}
                    </div>
                    <div  v-for="item in success" class="alert alert-success mt-2" role="alert">
                        @{{item}}
                    </div>
                    <table class="table">
                        <tr v-for="item in uploaded_list">
                            <td class="align-middle first-td">
                                <a href="Javascript:void(0)"  @click="view(item)">
                                    <img class="asset-thumb " v-if="item.type == 'application/pdf'" src="{{ asset('images/pdf.png') }}">
                                    <img class="asset-thumb " v-if="item.type == 'video/mp4'" src="{{ asset('images/youtube.png') }}">
                                    <img class="asset-thumb " v-if="item.type == 'image/jpeg'" :src="item.path">
                                </a>
                            </td>
                            <td class="align-middle">
                                <a href="Javascript:void(0)" @click="view(item)"> @{{ item.filename}}</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>
            <div class="modal fade bd-example-modal-lg" id="preview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class=" text-white">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img v-if="selected_asset.type == 'image/jpeg'" :src="selected_asset.path" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade bd-example-modal-lg"   data-backdrop="static" id="previewVideo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" @click="close" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class=" text-white">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <video  style="height: 500px"  id="my-video-stream" width="100%"   ref="video" controls @timeupdate="currentTime_ = $event.target.currentTime">
                                <source  id="mp4" :src="video" type="video/mp4">
                                <source :src="video"  type="video/ogg">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </user-file-upload>
@endsection

@section('page_stylesheets')
<style>
    .asset-thumb {
        width:50px;
    }
    .first-td {
      width: 70px;
    }
    .modal-content {
        background: none;
        box-shadow: none;
        border: 0px;
    }
    .modal-header {
        border-bottom: 0px;
    }
    .close {
        opacity: 1 !important;
    }
    .hide {
        display: none;  
    }
    .file-delete {
        color: white !important;
        margin-right: 10px;
        font-size: 20px;
    }
    .vue-file-agent .file-error-wrapper .file-error-message {
        padding: 3px;
    }
</style>
@endsection